package com.chupir.todolist.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.functions.Func1;

public class TodoModel implements Parcelable {

    public static final String TABLE = "tasks";

    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String COMMENT = "comment";
    public static final String TIMESTAMP = "timestamp";
    public static final String IS_COMPLETED = "isCompleted";
    public static final String IS_NOTIFY_ENABLED = "isNotifyEnabled";
    public static final String TIME_NOTIFY = "timeNotify";

    public static final Parcelable.Creator<TodoModel> CREATOR = new Parcelable.Creator<TodoModel>() {
        @Override
        @NonNull public TodoModel createFromParcel(@NonNull Parcel source) {
            return new TodoModel(source);
        }

        @Override
        @NonNull public TodoModel[] newArray(int size) {
            return new TodoModel[size];
        }
    };

    @Nullable private String id;
    @NonNull private String title;
    @NonNull private String comment;
    private long timestamp;
    private boolean isCompleted;
    private boolean isNotifyEnabled;
    private long timeNotify;

    public TodoModel(@Nullable
                     String id,
                     @NonNull
                     String title,
                     @NonNull
                     String comment,
                     long timestamp,
                     boolean isCompleted,
                     boolean isNotifyEnabled,
                     long timeNotify) {
        this.id = id;
        this.title = title;
        this.comment = comment;
        this.timestamp = timestamp;
        this.isCompleted = isCompleted;
        this.isNotifyEnabled = isNotifyEnabled;
        this.timeNotify = timeNotify;
    }

    public TodoModel(@NonNull Parcel in) {
        id = in.readString();
        title = in.readString();
        comment = in.readString();
        timestamp = in.readLong();
        boolean[] data = new boolean[2];
        in.readBooleanArray(data);
        isCompleted = data[0];
        isNotifyEnabled = data[1];
        timeNotify = in.readLong();
    }

    @Nullable
    public String getId() {
        return id;
    }

    @NonNull public String getTitle() {
        return title;
    }

    @NonNull public String getComment() {
        return comment;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public boolean isNotifyEnabled() {
        return isNotifyEnabled;
    }

    public long getTimeNotify() {
        return timeNotify;
    }

    public void setTimeNotify(long timeNotify) {
        this.timeNotify = timeNotify;
    }

    public void setNotifyEnabled(boolean notifyEnabled) {
        isNotifyEnabled = notifyEnabled;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(comment);
        dest.writeLong(timestamp);
        dest.writeBooleanArray(new boolean[] {isCompleted, isNotifyEnabled});
        dest.writeLong(timeNotify);
    }

    public ContentValues buildCV(){
        ContentValues cv = new ContentValues();
        cv.put(TITLE, title);
        cv.put(COMMENT, comment);
        cv.put(TIMESTAMP, timestamp);
        cv.put(IS_COMPLETED, isCompleted);
        cv.put(IS_NOTIFY_ENABLED, isNotifyEnabled);
        cv.put(TIME_NOTIFY, timeNotify);
        return cv;
    }


    public static Func1<Cursor, TodoModel> MAPPER = cursor -> {
        String id1 = String.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow(ID)));
        String title1 = cursor.getString(cursor.getColumnIndexOrThrow(TITLE));
        String comment1 = cursor.getString(cursor.getColumnIndexOrThrow(COMMENT));
        long timestamp1 = cursor.getLong(cursor.getColumnIndexOrThrow(TIMESTAMP));
        boolean isCompleted1 = cursor.getInt(cursor.getColumnIndexOrThrow(IS_COMPLETED)) == 1;
        boolean isNotifyEnabled = cursor.getInt(cursor.getColumnIndexOrThrow(IS_NOTIFY_ENABLED)) == 1;
        long timeNotify1 = cursor.getLong(cursor.getColumnIndexOrThrow(TIME_NOTIFY));
        return new TodoModel(id1, title1, comment1, timestamp1, isCompleted1, isNotifyEnabled, timeNotify1);
    };
}
