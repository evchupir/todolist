package com.chupir.todolist.model;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MockRepository {

    @NonNull public static List<TodoModel> getModels (int size) {
        List<TodoModel> models = new ArrayList<>(size);
        Random random = new Random();
        for (int i = 0; i < size; i++){
            models.add(new TodoModel("", "titles" + random.nextInt(size), "comments" + random.nextInt(size),
                    System.currentTimeMillis(), random.nextBoolean(), random.nextBoolean(), random.nextInt(size)));
        }
        return models;
    }
}
