package com.chupir.todolist.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.chupir.todolist.R;
import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.ui.screens.createtask.TaskActivity;


public class NotificationService extends IntentService {

    @NonNull public static final String NOTIFICATION_ID = "notification_id";
    @NonNull public static final String NOTIFICATION_TITLE = "notification_title";
    @NonNull public static final String NOTIFICATION_MODEL = "notification_model";

    public NotificationService() {
        super(NotificationService.class.getSimpleName());
    }

    private void sendNotif(@NonNull String message, int lastId, TodoModel model) {
        Intent intent = new Intent(this, TaskActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(TaskActivity.MODEL, model);
        PendingIntent pi = PendingIntent.getActivity(this, lastId,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this).
                setSmallIcon(R.mipmap.ic_launcher).
                setContentTitle(getResources().getString(R.string.app_name)).
                setContentText(message).
                setAutoCancel(true).
                setContentIntent(pi).
                setDefaults(Notification.DEFAULT_ALL);
        Notification notification = builder.build();
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(lastId, notification);
    }

    @Override
    protected void onHandleIntent(@NonNull Intent intent) {
        sendNotif(intent.getStringExtra(NOTIFICATION_TITLE),
                intent.getIntExtra(NOTIFICATION_ID, -1),
                intent.getParcelableExtra(NOTIFICATION_MODEL));
    }
}
