package com.chupir.todolist.ui.screens.list;

import android.support.annotation.NonNull;

import com.chupir.todolist.model.TodoModel;

import java.util.List;

public interface TodoListContract {
    interface TodoListView {
        void showTodoList(@NonNull List<TodoModel> todoList);
    }

    interface TodoListPresenter {
        void setView(@NonNull TodoListView view);
        void requestTodoList();
        void removeModel(@NonNull TodoModel model);
        void editNotify(@NonNull String id, boolean isNotify, long time);
        void editCompleted(@NonNull String id, boolean isCompleted);
    }
}
