package com.chupir.todolist.ui.screens.list;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

import com.chupir.todolist.R;
import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.ui.screens.createtask.TaskActivity;
import com.chupir.todolist.repository.db.DbOpenHelper;
import com.chupir.todolist.repository.db.DbRepository;
import com.chupir.todolist.ui.views.SwipeToDismissHelper;
import com.chupir.todolist.utils.AlarmUtils;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.util.Calendar;
import java.util.List;

import rx.schedulers.Schedulers;

public class TodoListActivity extends AppCompatActivity implements TodoListContract.TodoListView {

    @NonNull private TodoListAdapter adapter = new TodoListAdapter(this);
    @NonNull
    private SqlBrite sqlBrite = SqlBrite.create();
    @NonNull
    private DbOpenHelper openHelper = new DbOpenHelper(this);
    @NonNull
    private BriteDatabase db = sqlBrite.wrapDatabaseHelper(openHelper, Schedulers.io());
    @NonNull
    private TodoListContract.TodoListPresenter presenter = new DefaultTodoListPresenter(new DbRepository(db));

    @NonNull private Calendar myCalendar = Calendar.getInstance();

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_todo_list);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.todolist_activity_view);
        if (recyclerView != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(adapter);
        }
        adapter.setNotifyListener((checkBox, todoId, isChecked, position) -> {
            if (isChecked) {
                DatePickerDialog d = new DatePickerDialog(TodoListActivity.this, (view, year, monthOfYear, dayOfMonth) -> {
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                    TimePickerDialog t = new TimePickerDialog(TodoListActivity.this, (view1, hourOfDay, minute) -> {
                        myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        myCalendar.set(Calendar.MINUTE, minute);
                        adapter.setStatusNotify(position, true);
                        AlarmUtils.setServiceAlarm(this, true, myCalendar.getTimeInMillis(),
                                adapter.getModels().get(position),
                                Integer.parseInt(todoId));
                        presenter.editNotify(todoId, true, myCalendar.getTimeInMillis());
                        myCalendar.setTimeInMillis(System.currentTimeMillis());

                    }, myCalendar.get(Calendar.HOUR_OF_DAY),
                            myCalendar.get(Calendar.MINUTE), true);
                    t.setCanceledOnTouchOutside(false);
                    t.setOnCancelListener(dialog -> {
                        adapter.setStatusNotify(position, false);
                        checkBox.setChecked(false);
                        myCalendar.setTimeInMillis(System.currentTimeMillis());
                    });
                    t.show();
                }, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                d.setCanceledOnTouchOutside(false);
                d.setOnCancelListener(dialog -> {
                    adapter.setStatusNotify(position, false);
                    checkBox.setChecked(false);
                    myCalendar.setTimeInMillis(System.currentTimeMillis());
                });
                d.show();
            } else {
                AlarmUtils.setServiceAlarm(this, false, -1,
                        adapter.getModels().get(position),
                        Integer.parseInt(todoId));
                myCalendar.setTimeInMillis(System.currentTimeMillis());
                presenter.editNotify(todoId, false, -1);
            }
        });

        adapter.setCompleteListener((todoId, isChecked, position) -> {
                adapter.setStatusCompleted(position, isChecked);
                presenter.editCompleted(todoId, isChecked);
        });

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.todolist_fab);

        if (fab != null) {
            fab.setOnClickListener(v -> {
                Intent intent = new Intent(TodoListActivity.this, TaskActivity.class);
                startActivity(intent);
            });
        }

        presenter.setView(this);

        SwipeToDismissHelper swipeToDismissHelper = new SwipeToDismissHelper(new SwipeToDismissHelper.SwipeToDismissCallback(){

            @Override
            public void onSwipedLeft(RecyclerView.ViewHolder viewHolder) {
                Intent intent = new Intent(TodoListActivity.this, TaskActivity.class);
                intent.putExtra(TaskActivity.MODEL, adapter.getModels().get(viewHolder.getAdapterPosition()));
                intent.putExtra(TaskActivity.POSITION, viewHolder.getAdapterPosition());
                startActivity(intent);
            }

            @Override
            public void onSwipedRight(RecyclerView.ViewHolder viewHolder) {
                AlarmUtils.setServiceAlarm(TodoListActivity.this, false, -1,
                        adapter.getModels().get(viewHolder.getAdapterPosition()),
                        Integer.parseInt(adapter.getModels().get(viewHolder.getAdapterPosition()).getId()));
                initDialog(viewHolder);
            }
        });
        new ItemTouchHelper(swipeToDismissHelper).attachToRecyclerView(recyclerView);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.requestTodoList();
    }

    @Override
    public void showTodoList(@NonNull List<TodoModel> todoList) {
        adapter.setTodoModels(todoList);
    }

    private void initDialog(RecyclerView.ViewHolder viewHolder){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(R.string.dialog_delete_task);
        alertDialog.setPositiveButton(R.string.dialog_button_ok, (dialog, which) -> {
            presenter.removeModel(adapter.getModels().get(viewHolder.getAdapterPosition()));
            adapter.removeItem(viewHolder.getAdapterPosition());
        });
        alertDialog.setNegativeButton(R.string.dialog_button_cancel, (dialog, which) -> {
            adapter.notifyDataSetChanged();
        });
        alertDialog.show();
    }
}
