package com.chupir.todolist.ui.views;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.chupir.todolist.R;

public class SwipeToDismissHelper extends ItemTouchHelper.SimpleCallback {

    @NonNull
    public SwipeToDismissCallback callback;
    @NonNull
    private Paint p = new Paint();

    public SwipeToDismissHelper(@NonNull SwipeToDismissCallback callback) {
        super( ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
        this.callback = callback;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder,
                          @NonNull RecyclerView.ViewHolder target) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null){
            return false;
        }
        adapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        if (direction == ItemTouchHelper.LEFT) {
            callback.onSwipedLeft(viewHolder);

        } else {
            if (direction == ItemTouchHelper.RIGHT) {
                callback.onSwipedRight(viewHolder);
            }
        }
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView,
                            @NonNull RecyclerView.ViewHolder viewHolder,
                            float dX, float dY, int actionState, boolean isCurrentlyActive) {

        Bitmap icon;
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            View itemView = viewHolder.itemView;
            float height = itemView.getBottom() - itemView.getTop();
            float width = height/3;

            if (dX > 0) {
                p.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.red_f));
                RectF background = new RectF(itemView.getLeft(), itemView.getTop(),
                        dX, itemView.getBottom());
                c.drawRect(background, p);
                icon = BitmapFactory.decodeResource(recyclerView.getContext().getResources(), R.mipmap.ic_delete_white);
                RectF icon_dest = new RectF(itemView.getLeft() + width, itemView.getTop() + width,
                        itemView.getLeft() + 2 * width, itemView.getBottom() - width);
                c.drawBitmap(icon, null, icon_dest, p);
            } else {
                if (dX < 0) {
                    p.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.green_f));
                    RectF background = new RectF(itemView.getRight() + dX, itemView.getTop(),
                            itemView.getRight(), itemView.getBottom());
                    c.drawRect(background, p);
                    icon = BitmapFactory.decodeResource(recyclerView.getContext().getResources(), R.mipmap.ic_edit_white);
                    RectF icon_dest = new RectF(itemView.getRight() - 2 * width, itemView.getTop() + width,
                            itemView.getRight() - width, itemView.getBottom() - width);
                    c.drawBitmap(icon, null, icon_dest, p);
                }
            }
        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    public interface SwipeToDismissCallback {
        void onSwipedLeft(RecyclerView.ViewHolder viewHolder);
        void onSwipedRight(RecyclerView.ViewHolder viewHolder);
    }
}
