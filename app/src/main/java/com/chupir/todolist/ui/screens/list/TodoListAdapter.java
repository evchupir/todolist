package com.chupir.todolist.ui.screens.list;

import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.chupir.todolist.R;
import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.utils.AlarmUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TodoListAdapter extends RecyclerView.Adapter<TodoListAdapter.ViewHolder> {

    @NonNull private List<TodoModel> models = new ArrayList<>();
    @NonNull SimpleDateFormat sdf = new SimpleDateFormat("dd MMM", Locale.getDefault());
    @NonNull private Context context;
    @Nullable private OnTodoNotifyClickListener notifyListener;
    @Nullable private OnTodoCompleteClickListener completeListener;
    final long MS_IN_DAY = 86400000;

    public TodoListAdapter(@NonNull Context context) {
        this.context = context;
    }

    @NonNull
    public List<TodoModel> getModels() {
        return models;
    }

    public void setTodoModels(@NonNull List<TodoModel> models){
        this.models = models;
        Collections.sort(models, (lhs, rhs) ->
                String.valueOf(lhs.getTimestamp()).compareTo(String.valueOf(rhs.getTimestamp())));
        notifyDataSetChanged();
    }

    public void setNotifyListener(@NonNull OnTodoNotifyClickListener listener){
        this.notifyListener = listener;
    }

    public void setCompleteListener(@NonNull OnTodoCompleteClickListener listener){
        this.completeListener = listener;
    }

    @Override
    @NonNull public TodoListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_todo_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View itemView = holder.itemView;
        holder.tvTitle.setText(models.get(position).getTitle());
        holder.cbDate.setChecked(models.get(position).isCompleted());
        holder.tvComment.setText(models.get(position).getComment());
        SpannableString ss1 = new SpannableString(sdf.format(new Date(models.get(position).getTimestamp())));
        ss1.setSpan(new RelativeSizeSpan(2f), 0, 2, 0);
        holder.cbDate.setText(ss1);
        if (models.get(position).getTimestamp() + MS_IN_DAY < System.currentTimeMillis()) {
            if (!models.get(position).isCompleted()) {
                holder.tvTitle.setPaintFlags(holder.tvTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_lines_is_over));
                } else {
                    itemView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.item_lines_is_over));
                }
            } else {
                holder.tvTitle.setPaintFlags(holder.tvTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_lines_on));
                } else {
                    itemView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.item_lines_on));
                }
            }
        } else {
            if (!models.get(position).isCompleted()) {
                holder.tvTitle.setPaintFlags(holder.tvTitle.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_lines));
                } else {
                    itemView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.item_lines));
                }
            } else {
                holder.tvTitle.setPaintFlags(holder.tvTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    itemView.setBackground(ContextCompat.getDrawable(context, R.drawable.item_lines_on));
                } else {
                    itemView.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.item_lines_on));
                }
            }
        }
        if (models.get(position).getTimeNotify() < System.currentTimeMillis()) {
            holder.cbNotify.setChecked(false);
            AlarmUtils.setServiceAlarm(context, false, -1,
                    models.get(position), Integer.parseInt(models.get(position).getId()));
            models.get(position).setTimeNotify(-1);
        } else {
            holder.cbNotify.setChecked(true);
            AlarmUtils.setServiceAlarm(context, true, models.get(position).getTimeNotify(),
                    models.get(position), Integer.parseInt(models.get(position).getId()));
        }
        holder.cbNotify.setOnClickListener(v -> {
            String id = models.get(position).getId();
            if (notifyListener != null && id != null) {
                notifyListener.notifyCheckListener(holder.cbNotify, id, holder.cbNotify.isChecked(), position);
            }
        });
        holder.cbDate.setOnClickListener(v -> {
            String id = models.get(position).getId();
            if (completeListener != null && id != null) {
                completeListener.completeCheckListener(id, holder.cbDate.isChecked(), position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public void removeItem(int position) {
        models.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, models.size());
    }

    public void setStatusNotify(int position, boolean isChecked) {
        models.get(position).setNotifyEnabled(isChecked);
        notifyItemChanged(position);
    }

    public void setStatusCompleted(int position, boolean isChecked) {
        models.get(position).setCompleted(isChecked);
        notifyItemChanged(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @NonNull private CheckBox cbDate, cbNotify;
        @NonNull private TextView tvTitle, tvComment;

        public ViewHolder(@NonNull View v) {
            super(v);
            tvTitle = (TextView) v.findViewById(R.id.recycler_item_tvTitle);
            cbDate = (CheckBox) v.findViewById(R.id.recycler_item_cbDate);
            tvComment = (TextView) v.findViewById(R.id.recycler_item_tvComment);
            cbNotify = (CheckBox) v.findViewById(R.id.recycler_item_cbNotify);
        }
    }

    public interface OnTodoNotifyClickListener {
        void notifyCheckListener(@NonNull CheckBox checkBox,
                                 @NonNull String todoId,
                                 boolean isChecked,
                                 int position);
    }

    public interface OnTodoCompleteClickListener {
        void completeCheckListener(@NonNull String todoId,
                                   boolean isChecked,
                                   int position);
    }

}
