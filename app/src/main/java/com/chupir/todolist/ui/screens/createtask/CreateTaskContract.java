package com.chupir.todolist.ui.screens.createtask;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.chupir.todolist.model.TodoModel;

public interface CreateTaskContract {
    interface CreateTaskView {
        void showError();
        void closeScreen();
        void editItem(@NonNull TodoModel model);
    }

    interface CreateTaskPresenter {
        void setView(@NonNull CreateTaskView view, @NonNull Bundle extras);
        void createTask(@NonNull TodoModel model);
        void editModel(@NonNull String id, @NonNull TodoModel model);
        @Nullable TodoModel getModel();
    }
}
