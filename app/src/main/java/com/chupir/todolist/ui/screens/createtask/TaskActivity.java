package com.chupir.todolist.ui.screens.createtask;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.chupir.todolist.R;
import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.repository.db.DbOpenHelper;
import com.chupir.todolist.repository.db.DbRepository;
import com.chupir.todolist.ui.screens.list.TodoListActivity;
import com.squareup.sqlbrite.BriteDatabase;
import com.squareup.sqlbrite.SqlBrite;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import rx.schedulers.Schedulers;

public class TaskActivity extends AppCompatActivity implements CreateTaskContract.CreateTaskView {

    @NonNull public static final String MODEL = "model";
    @NonNull public static final String POSITION = "position";
    @Nullable private EditText etTitle, etComment, etDate, etNotify;
    @NonNull private Calendar myCalendar = Calendar.getInstance();
    @NonNull private SimpleDateFormat sdfD = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    @NonNull private SimpleDateFormat sdfN = new SimpleDateFormat("EEE, d MMM yyyy HH:mm", Locale.getDefault());

    @NonNull SqlBrite sqlBrite = SqlBrite.create();
    @NonNull DbOpenHelper openHelper = new DbOpenHelper(this);
    @NonNull BriteDatabase db = sqlBrite.wrapDatabaseHelper(openHelper, Schedulers.io());
    @NonNull final CreateTaskContract.CreateTaskPresenter presenter = new DefaultCreateTaskPresenter(new DbRepository(db));

    @NonNull DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(@NonNull DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (etDate != null) {
                etDate.setText(sdfD.format(myCalendar.getTime()));
            }
            myCalendar.setTimeInMillis(System.currentTimeMillis());
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        etTitle = (EditText) findViewById(R.id.task_item_etTitle);
        etComment = (EditText) findViewById(R.id.task_item_etComment);
        etDate = (EditText) findViewById(R.id.task_item_etDate);
        etNotify = (EditText) findViewById(R.id.task_item_etNotify);
        Button btnOk = (Button) findViewById(R.id.task_item_btnOk);

        presenter.setView(this, getIntent().getExtras());

        if (btnOk != null) {
            btnOk.setOnClickListener(v -> {
                try {
                    if (presenter.getModel() != null && presenter.getModel().getId() != null) {
                        presenter.editModel(presenter.getModel().getId(), buildTodoModel());
                    } else {
                        presenter.createTask(buildTodoModel());
                    }
                } catch (ParseException | IllegalStateException e) {
                    e.printStackTrace();
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter.getModel() != null) {
            editItem(presenter.getModel());
        } else {
            if (etDate != null && etNotify != null) {
                etDate.setText(sdfD.format(myCalendar.getTime()));
                etDate.setInputType(InputType.TYPE_NULL);
                etDate.setOnClickListener(v -> new DatePickerDialog(TaskActivity.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show());
                etNotify.setInputType(InputType.TYPE_NULL);
                etNotify.setOnClickListener(v -> editNotify());
            }
        }
    }

    private void editNotify() {
        DatePickerDialog d = new DatePickerDialog(TaskActivity.this, (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            TimePickerDialog t = new TimePickerDialog(TaskActivity.this, (view1, hourOfDay, minute) -> {
                myCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                myCalendar.set(Calendar.MINUTE, minute);
                if (etNotify != null) {
                    etNotify.setText(sdfN.format(myCalendar.getTimeInMillis()));
                }

            }, myCalendar.get(Calendar.HOUR_OF_DAY),
                    myCalendar.get(Calendar.MINUTE), true);
            t.setCanceledOnTouchOutside(false);
            t.setOnCancelListener(dialog -> myCalendar.setTimeInMillis(System.currentTimeMillis()));
            t.show();
        }, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        d.setCanceledOnTouchOutside(false);
        d.setOnCancelListener(dialog -> myCalendar.setTimeInMillis(System.currentTimeMillis()));
        d.show();
    }

    @Override
    protected void onNewIntent(@NonNull Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        presenter.setView(this, intent.getExtras());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        View v = getCurrentFocus();
        if (v != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
        return true;
    }

    @Override
    public void editItem(@NonNull TodoModel model) {
        if (etDate != null && etTitle != null &&
                etComment != null && etNotify != null) {
            Toolbar toolbar = (Toolbar) findViewById(R.id.task_toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle(R.string.task_activity_title_edit);
            }
            etTitle.setText(model.getTitle());
            etComment.setText(model.getComment());
            etDate.setText(sdfD.format(model.getTimestamp()));
            etDate.setInputType(InputType.TYPE_NULL);
            etDate.setOnClickListener(v -> new DatePickerDialog(TaskActivity.this, date, myCalendar.get(Calendar.YEAR),
                    myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show());
            if (model.getTimeNotify() != -1) {
                etNotify.setText(sdfN.format(model.getTimeNotify()));
            } else {
                etNotify.setText("");
            }
            etNotify.setInputType(InputType.TYPE_NULL);
            etNotify.setOnClickListener(v -> editNotify());
        }
    }

    @NonNull public TodoModel buildTodoModel() throws ParseException, IllegalStateException {
        boolean isNotify = false;
        if (etDate != null && etTitle != null && etComment != null && etNotify != null) {
            Date d = sdfD.parse(etDate.getText().toString());
            long timeNotify = -1;
            if (!TextUtils.isEmpty(etNotify.getText())) {
                Date n = sdfN.parse(etNotify.getText().toString());
                timeNotify = n.getTime();
                isNotify = true;
            }
            return new TodoModel("", etTitle.getText().toString(), etComment.getText().toString(), d.getTime(), false, isNotify, timeNotify);
        } else {
            throw new IllegalStateException("input view is null");
        }
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.task_activity_create_title, Toast.LENGTH_LONG).show();
    }

    @Override
    public void closeScreen() {
        Intent intent = new Intent(TaskActivity.this, TodoListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        TaskActivity.this.startActivity(intent);
        finish();
    }
}
