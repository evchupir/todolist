package com.chupir.todolist.ui.screens.createtask;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.repository.db.DbRepository;


public class DefaultCreateTaskPresenter implements CreateTaskContract.CreateTaskPresenter {

    private final String LOG_TAG = "myLogs";
    @Nullable private CreateTaskContract.CreateTaskView view;
    @Nullable Bundle extras;
    @NonNull DbRepository repository;

    public DefaultCreateTaskPresenter(@NonNull DbRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setView(@NonNull CreateTaskContract.CreateTaskView view, @NonNull Bundle extras) {
        this.view = view;
        this.extras = extras;
    }

    @Override
    public void createTask(@NonNull TodoModel model) {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }

        if (TextUtils.isEmpty(model.getTitle()) || TextUtils.isEmpty(String.valueOf(model.getTimestamp()))) {
            view.showError();
        } else {
            repository.addTask(model).subscribe(aLong -> {
                if (view != null) {
                    view.closeScreen();
                }
            }, throwable -> {
                if (view != null) {
                    view.showError();
                    Log.d(LOG_TAG, "add task error");
                }
            });
        }
    }

    @Override
    public void editModel(@NonNull String id, @NonNull TodoModel model) {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }

        if (TextUtils.isEmpty(model.getTitle()) || TextUtils.isEmpty(String.valueOf(model.getTimestamp()))) {
            view.showError();
        } else {
            repository.updateTasks(id, model.buildCV()).subscribe(aLong -> {
                if (view != null) {
                    view.closeScreen();
                }
            }, throwable -> {
                Log.d(LOG_TAG, "error updating task " + throwable.getLocalizedMessage());

            });
        }
    }

    @Nullable
    @Override
    public TodoModel getModel() {
        if(extras != null) {
           return extras.getParcelable(TaskActivity.MODEL);
        } else
        return null;
    }
}
