package com.chupir.todolist.ui.screens.list;

import android.content.ContentValues;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.repository.db.DbRepository;

public class DefaultTodoListPresenter implements TodoListContract.TodoListPresenter {

    private final String LOG_TAG = "myLogs";
    @Nullable private TodoListContract.TodoListView view;
    @NonNull DbRepository repository;

    public DefaultTodoListPresenter(@NonNull DbRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setView(@NonNull TodoListContract.TodoListView view) {
        this.view = view;
    }

    @Override
    public void requestTodoList() {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }
        repository.getAllTasks().subscribe(models -> {
            if (view != null) {
                view.showTodoList(models);
            }
        }, throwable -> {
            Log.d(LOG_TAG, "error while fetching tasks");
        });
    }

    @Override
    public void removeModel(@NonNull TodoModel model) {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }
        repository.deleteTask(model).subscribe(aInt -> {
            if(aInt > 0 && view != null){
                requestTodoList();
            }
        }, throwable -> {
            Log.d(LOG_TAG, "error while removing tasks");
        });
    }

    @Override
    public void editNotify(@NonNull String id, boolean isNotify, long time) {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }
        ContentValues cv = new ContentValues();
        cv.put(TodoModel.ID, id);
        cv.put(TodoModel.IS_NOTIFY_ENABLED, isNotify);
        cv.put(TodoModel.TIME_NOTIFY, time);
        repository.updateTasks(id, cv).subscribe(aLong -> {
        }, throwable -> {
                Log.d(LOG_TAG, "error updating task " + throwable.getLocalizedMessage());

        });
    }

    @Override
    public void editCompleted(@NonNull String id, boolean isCompleted) {
        if (view == null) {
            throw new IllegalStateException("view is null, you should set view before");
        }
        ContentValues cv = new ContentValues();
        cv.put(TodoModel.ID, id);
        cv.put(TodoModel.IS_COMPLETED, isCompleted);
        repository.updateTasks(id, cv).subscribe(aLong -> {
        }, throwable -> {
            Log.d(LOG_TAG, "error updating task " + throwable.getLocalizedMessage());

        });
    }
}
