package com.chupir.todolist.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.chupir.todolist.model.TodoModel;
import com.chupir.todolist.services.NotificationService;

public class AlarmUtils {
    public static void setServiceAlarm(@NonNull Context context, boolean isOn, long time, TodoModel model, int lastId) {
        Intent i = new Intent(context, NotificationService.class);
        i.putExtra(NotificationService.NOTIFICATION_ID, lastId);
        i.putExtra(NotificationService.NOTIFICATION_TITLE, model.getTitle());
        i.putExtra(NotificationService.NOTIFICATION_MODEL, model);
        PendingIntent pi = PendingIntent.getService(context, lastId, i, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)
                context.getSystemService(Context.ALARM_SERVICE);
        if (isOn) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pi);
        } else {
            alarmManager.cancel(pi);
            pi.cancel();
        }
    }
}
