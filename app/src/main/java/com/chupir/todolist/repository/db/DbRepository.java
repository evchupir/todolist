package com.chupir.todolist.repository.db;

import android.content.ContentValues;
import android.database.SQLException;
import android.support.annotation.NonNull;

import com.chupir.todolist.model.TodoModel;
import com.squareup.sqlbrite.BriteDatabase;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DbRepository {

    @NonNull private final BriteDatabase briteDatabase;

    public DbRepository(@NonNull BriteDatabase briteDatabase) {
        this.briteDatabase = briteDatabase;
    }

    @NonNull
    public Observable<Long> addTask(@NonNull TodoModel todoModel) {
        return Observable.create(new Observable.OnSubscribe<Long>() {
            @Override
            public void call(Subscriber<? super Long> subscriber) {
                long result = briteDatabase.insert(TodoModel.TABLE, todoModel.buildCV());
                if (result == -1) {
                    subscriber.onError(new SQLException("error inserting task with title " + todoModel.getId()));
                } else {
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    public Observable<List<TodoModel>> getAllTasks() {
        return briteDatabase
                .createQuery(TodoModel.TABLE, "SELECT * FROM " + TodoModel.TABLE, new String[]{})
                .mapToList(TodoModel.MAPPER)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    public Observable<Integer> updateTasks(@NonNull String id, @NonNull ContentValues cv) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                int result = briteDatabase.update(TodoModel.TABLE,
                        cv, TodoModel.ID + "=?", id);

                if (result == 0) {
                    subscriber.onError(new SQLException("error updating tasks"));
                } else {
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @NonNull
    public Observable<Integer> deleteTask(@NonNull TodoModel todoModel) {
        return Observable.create(new Observable.OnSubscribe<Integer>() {
            @Override
            public void call(Subscriber<? super Integer> subscriber) {
                int result = briteDatabase.delete(TodoModel.TABLE, TodoModel.ID + "=?", todoModel.getId());

                if (result == 0) {
                    subscriber.onError(new SQLException("error deleting task"));
                } else {
                    subscriber.onNext(result);
                    subscriber.onCompleted();
                }
            }
        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
