package com.chupir.todolist.repository.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.chupir.todolist.model.TodoModel;

public class DbOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "models.db";
    private static final int DB_VERSION = 1;

    private static final String CREATE_TASKS = ""
            + "CREATE TABLE " + TodoModel.TABLE + "("
            + TodoModel.ID + " INTEGER NOT NULL PRIMARY KEY,"
            + TodoModel.TITLE + " TEXT NOT NULL,"
            + TodoModel.COMMENT + " TEXT,"
            + TodoModel.TIMESTAMP + " TEXT NOT NULL,"
            + TodoModel.IS_COMPLETED + " INTEGER,"
            + TodoModel.IS_NOTIFY_ENABLED + " INTEGER,"
            + TodoModel.TIME_NOTIFY + " TEXT"
            + ")";

    public DbOpenHelper(@NonNull Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TASKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
